package com.xideral.bootTest.controller;

import com.xideral.bootTest.dto.request.create.NewUserRequest;
import com.xideral.bootTest.dto.request.update.UpdateUserRequest;
import com.xideral.bootTest.dto.resources.UserResource;
import com.xideral.bootTest.entities.User;
import com.xideral.bootTest.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/users")
public class UserController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<?> list(){
        LOG.info("list users");

        List<UserResource> users = userService.findAll();
        return new ResponseEntity<List<UserResource>>(users, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserResource> read(@PathVariable Integer id){
        LOG.info("read() id: {}", id);
        User user = userService.retrieve(id);
        return new ResponseEntity<UserResource>(new UserResource(user), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody NewUserRequest request, BindingResult result){
        if(result.hasErrors()) {
            return validate(result);
        }
        LOG.info("create() request: {}", request);
        User user = userService.persist(request);
        return new ResponseEntity<UserResource>(new UserResource(user), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Integer id, @Valid @RequestBody UpdateUserRequest request, BindingResult result){
        if(result.hasErrors()) {
            return validate(result);
        }
        LOG.info("update() id: {}", id);
        request.setId(id);
        LOG.info("request: {}", request);
        User user = userService.update(request);
        return new ResponseEntity<UserResource>(new UserResource(user), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<UserResource> remove(@PathVariable Integer id){
        LOG.info("remove() id: {}", id);
        userService.remove(id);
        return new ResponseEntity<UserResource>(new UserResource(HttpStatus.NO_CONTENT.getReasonPhrase(), HttpStatus.NO_CONTENT.value()), HttpStatus.OK);
    }

    public ResponseEntity<?> validate(BindingResult result) {
        Map<String, String> errors = new HashMap<>(0);
        for(FieldError error: result.getFieldErrors()) {
            errors.put(error.getField(), "El campo " + error.getField() + " " + error.getDefaultMessage());
        }
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    private static final Logger LOG = LoggerFactory.getLogger(UserController.class);

}
