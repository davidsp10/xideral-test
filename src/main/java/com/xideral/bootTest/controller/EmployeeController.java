package com.xideral.bootTest.controller;

import com.xideral.bootTest.dto.request.create.NewEmployeeRequest;
import com.xideral.bootTest.dto.request.update.UpdateEmployeeRequest;
import com.xideral.bootTest.dto.resources.EmployeeResource;
import com.xideral.bootTest.entities.Employee;
import com.xideral.bootTest.service.EmployeeService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RequiredArgsConstructor
@RestController
@RequestMapping("/api/employees")
public class EmployeeController {

    private final EmployeeService employeeService;

    @GetMapping
    public ResponseEntity<?> list(){
        LOG.info("list employees");

        List<EmployeeResource> employees = employeeService.findAll();
        return new ResponseEntity<List<EmployeeResource>>(employees, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeResource> read(@PathVariable Integer id){
        LOG.info("read() id: {}", id);
        Employee employee = employeeService.retrieve(id);
        return new ResponseEntity<EmployeeResource>(new EmployeeResource(employee), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody NewEmployeeRequest request, BindingResult result) throws NoSuchAlgorithmException {
        if(result.hasErrors()) {
            return validate(result);
        }
        LOG.info("create() request: {}", request);
        Employee employee = employeeService.persist(request);
        return new ResponseEntity<EmployeeResource>(new EmployeeResource(employee), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Integer id, @Valid @RequestBody UpdateEmployeeRequest request, BindingResult result) throws NoSuchAlgorithmException {
        if(result.hasErrors()) {
            return validate(result);
        }
        LOG.info("update() id: {}", id);
        request.setId(id);
        LOG.info("request: {}", request);
        Employee employee = employeeService.update(request);
        return new ResponseEntity<EmployeeResource>(new EmployeeResource(employee), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<EmployeeResource> remove(@PathVariable Integer id){
        LOG.info("remove() id: {}", id);
        employeeService.remove(id);
        return new ResponseEntity<EmployeeResource>(new EmployeeResource(HttpStatus.NO_CONTENT.getReasonPhrase(), HttpStatus.NO_CONTENT.value()), HttpStatus.OK);
    }

    public ResponseEntity<?> validate(BindingResult result) {
        Map<String, String> errors = new HashMap<>(0);
        for(FieldError error: result.getFieldErrors()) {
            errors.put(error.getField(), "El campo " + error.getField() + " " + error.getDefaultMessage());
        }
        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);

}
