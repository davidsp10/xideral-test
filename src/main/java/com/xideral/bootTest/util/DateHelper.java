package com.xideral.bootTest.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateHelper {
	public static String tranformToString(Date date, String format) {
		try {
			DateFormat df = new SimpleDateFormat(format, new Locale("es", "MX"));
			return df.format(date);
		} catch (Exception e) {
			LOG.error("Error in get Date, value={}, format = {}", date, format);
			return null;
		}
	}

	private static final Logger LOG = LoggerFactory.getLogger(DateHelper.class);
}
