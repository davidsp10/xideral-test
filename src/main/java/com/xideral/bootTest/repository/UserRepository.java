package com.xideral.bootTest.repository;

import com.xideral.bootTest.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Integer> {

    List<User> findAllByEmployeeIsUserIsTrue();
}
