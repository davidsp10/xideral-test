package com.xideral.bootTest.repository;

import com.xideral.bootTest.entities.JobAreaUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface JobAreaUserRepository extends JpaRepository<JobAreaUser, Integer> {

    Optional<JobAreaUser> findByJobAreaIdAndUserId(Integer jobAreaId, Integer userId);

}
