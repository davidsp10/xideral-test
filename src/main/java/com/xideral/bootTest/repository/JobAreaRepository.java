package com.xideral.bootTest.repository;

import com.xideral.bootTest.entities.JobArea;
import org.springframework.data.jpa.repository.JpaRepository;

public interface JobAreaRepository extends JpaRepository<JobArea, Integer> {
}
