package com.xideral.bootTest.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@ToString
@Entity
@EqualsAndHashCode(of = {"id"})
@Table(name = "cat_job_area_user", schema = "public")
public class JobAreaUser {

    @Id
    @Column(name = "joau_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "usr_id", nullable = false)
    private User user;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY) @JoinColumn(name = "joa_id", nullable = false)
    private JobArea jobArea;

}
