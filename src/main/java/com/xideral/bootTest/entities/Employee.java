package com.xideral.bootTest.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.Date;

@Data
@ToString
@Entity
@EqualsAndHashCode(of = {"id"})
@Table(name = "cat_employee", schema = "public")
public class Employee {

    @Id
    @Column(name = "emp_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "emp_name")
    private String name;

    @Column(name = "emp_first_name")
    private String firstName;

    @Column(name = "emp_last_name")
    private String lastName;

    @Column(name = "emp_birth_date")
    private Date birthDate;

    @Column(name = "acceso_sistema")
    private Boolean isUser;

    @OneToOne(mappedBy = "employee", cascade = CascadeType.ALL)
    private User user;

}
