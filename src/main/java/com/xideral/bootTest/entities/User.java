package com.xideral.bootTest.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.springframework.util.CollectionUtils;

import java.util.HashSet;
import java.util.Set;

@Data
@ToString
@Entity
@EqualsAndHashCode(of = {"id"})
@Table(name = "cat_user", schema = "public")
public class User {

    @Id
    @Column(name = "usr_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "usr_name", nullable = false)
    private String name;

    @Column(name = "usr_email",  unique = true, nullable = false)
    private String email;

    @Column(name = "usr_password", nullable = false)
    private String password;

    @ToString.Exclude
    @OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.PERSIST, CascadeType.REFRESH })
    @JoinColumn(name = "emp_id", nullable = false)
    private Employee employee;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<JobAreaUser> jobAreasUser = new HashSet<>(0);

    public void addJobAreaUser(JobAreaUser jobAreaUser) {
        this.jobAreasUser.add(jobAreaUser);
    }

    public boolean removeJobAreaUser(JobAreaUser jobAreasUser) {
        return this.jobAreasUser.remove(jobAreasUser);
    }

}
