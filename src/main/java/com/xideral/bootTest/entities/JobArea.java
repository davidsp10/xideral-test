package com.xideral.bootTest.entities;

import jakarta.persistence.*;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.util.HashSet;
import java.util.Set;

@Data
@ToString
@Entity
@EqualsAndHashCode(of = {"id"})
@Table(name = "cat_job_area", schema = "public")
public class JobArea {

    @Id
    @Column(name = "joa_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "joa_name")
    private String name;

    @Column(name = "joa_abbreviation")
    private String abbreviation;

    @OneToMany(mappedBy = "jobArea")
    private Set<JobAreaUser> users = new HashSet<>(0);

}
