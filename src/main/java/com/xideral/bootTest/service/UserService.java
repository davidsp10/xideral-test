package com.xideral.bootTest.service;

import com.xideral.bootTest.dto.request.create.NewUserRequest;
import com.xideral.bootTest.dto.request.update.UpdateUserRequest;
import com.xideral.bootTest.dto.resources.UserResource;
import com.xideral.bootTest.entities.User;

import java.util.List;

public interface UserService {

    List<UserResource> findAll();

    User retrieve(Integer id);

    User persist(NewUserRequest request);

    User update(UpdateUserRequest request);

    void remove(Integer id);

}
