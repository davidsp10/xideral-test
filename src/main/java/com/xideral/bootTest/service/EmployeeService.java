package com.xideral.bootTest.service;

import com.xideral.bootTest.dto.request.create.NewEmployeeRequest;
import com.xideral.bootTest.dto.request.update.UpdateEmployeeRequest;
import com.xideral.bootTest.dto.resources.EmployeeResource;
import com.xideral.bootTest.entities.Employee;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface EmployeeService {

    List<EmployeeResource> findAll();

    Employee retrieve(Integer id);

    Employee persist(NewEmployeeRequest request) throws NoSuchAlgorithmException;

    Employee update(UpdateEmployeeRequest request) throws NoSuchAlgorithmException;

    void remove(Integer id);

}
