package com.xideral.bootTest.service;

import com.xideral.bootTest.entities.JobArea;

public interface JobAreaService {

    JobArea retrieve(Integer id);

}
