package com.xideral.bootTest.service.impl;

import com.xideral.bootTest.dto.request.create.NewUserRequest;
import com.xideral.bootTest.dto.request.update.UpdateUserRequest;
import com.xideral.bootTest.dto.resources.UserResource;
import com.xideral.bootTest.entities.Employee;
import com.xideral.bootTest.entities.JobArea;
import com.xideral.bootTest.entities.JobAreaUser;
import com.xideral.bootTest.entities.User;
import com.xideral.bootTest.repository.EmployeeRepository;
import com.xideral.bootTest.repository.JobAreaRepository;
import com.xideral.bootTest.repository.JobAreaUserRepository;
import com.xideral.bootTest.repository.UserRepository;
import com.xideral.bootTest.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;
import java.util.stream.Collectors;

import static com.xideral.bootTest.constants.MessageConstanst.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final EmployeeRepository employeeRepository;
    private final JobAreaRepository jobAreaRepository;
    private final JobAreaUserRepository jobAreaUserRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional(readOnly = true)
    public List<UserResource> findAll() {
        LOG.debug("findAll()");
        List<UserResource> userResourceList = new ArrayList<>(0);
        for(User user: userRepository.findAllByEmployeeIsUserIsTrue()) {
            userResourceList.add(new UserResource(user));
        }
        return userResourceList;
    }

    @Override
    @Transactional(readOnly = true)
    public User retrieve(Integer id) {
        return  userRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
    }

    @Transactional(readOnly = true)
    public Employee retrieveEmployee(Integer id) {
        return employeeRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, EMPLOYEE_NOT_FOUND));
    }

    @Override
    @Transactional
    public User persist(NewUserRequest request) {
        LOG.debug("persist() request: {}", request);

        User user = new User();
        user.setName(request.getName());
        user.setPassword(passwordEncoder.encode(request.getHash()));
        user.setEmail(request.getEmail());

        Employee employee = new Employee();
        employee.setName(request.getName());
        employee.setIsUser(Boolean.TRUE);
        user.setEmployee(employee);

        Set<JobAreaUser> jobAreaUsers = new HashSet<>(0);
        if(!request.getJobAreas().isEmpty()) {
            for (Integer jobAreaId : request.getJobAreas()) {
                JobArea jobArea = jobAreaRepository.findById(jobAreaId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, JOB_AREA_NOT_FOUND));
                if (jobArea != null && jobArea.getId() != null) {
                    JobAreaUser jobAreaUser = new JobAreaUser();
                    jobAreaUser.setId(Integer.valueOf(RandomStringUtils.randomAlphanumeric(0, 1000000)));
                    jobAreaUser.setUser(user);
                    jobAreaUser.setJobArea(jobArea);
                    jobAreaUsers.add(jobAreaUser);
                } else {
                    throw new ResponseStatusException(HttpStatus.NOT_FOUND, JOB_AREA_NOT_FOUND);
                }
            }
        }

        user.setJobAreasUser(jobAreaUsers);
        userRepository.save(user);

        if(user.getId() != null)
            LOG.info("User {} created", user.getName());
        else
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Conflict creating User");

        return user;
    }

    @Override
    @Transactional
    public User update(UpdateUserRequest request) {
        LOG.debug("update() request: {}", request);

        User user = this.retrieve(request.getId());
        user.setName(request.getName());
        user.setEmail(request.getEmail());

        Employee employee =this.retrieveEmployee(user.getEmployee().getId());
        employee.setName(request.getName());
        user.setEmployee(employee);

        this.updateJobAreasUserList(user, request);

        userRepository.save(user);

        if(user.getId() != null)
            LOG.info("User {} updated", user.getName());
        else
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Conflict updating User");

        return user;
    }

    @Override
    @Transactional
    public void remove(Integer id) {
        LOG.debug("remove() id: {}", id);
        User user = this.retrieve(id);
        userRepository.delete(user);
    }

    private void updateJobAreasUserList(User user, UpdateUserRequest userRequest) {
        LOG.info("updateJobAreasUserList() request {}:", userRequest.getJobAreas());

        List<Integer> currentJobAreas     = user.getJobAreasUser().stream().map(JobAreaUser::getJobArea).map(JobArea::getId).collect(Collectors.toList());

        List<Integer> jobAreaListToDelete = currentJobAreas.stream()
                .filter(jobAreaId -> !userRequest.getJobAreas().contains(jobAreaId))
                .collect(Collectors.toList());

        List<Integer> jobAreaListToCreate = userRequest.getJobAreas().stream()
                .filter(jobAreaId -> !currentJobAreas.contains(jobAreaId))
                .collect(Collectors.toList());

        this.removeJobAreasUserList(user, jobAreaListToDelete);
        this.addJobAreasUserList(user, jobAreaListToCreate);
    }

    private void removeJobAreasUserList(User user, List<Integer> jobAreaListToDelete) {
        LOG.info("removeJobAreasUserList {}", jobAreaListToDelete);

        Iterator<Integer> iterator = jobAreaListToDelete.iterator();
        while (iterator.hasNext()) {
            JobAreaUser jobAreaUser = jobAreaUserRepository.findByJobAreaIdAndUserId(iterator.next(), user.getId())
                    .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, JOB_AREA_USER_NOT_FOUND));
            user.removeJobAreaUser(jobAreaUser);
        }
    }

    private void addJobAreasUserList(User user, List<Integer> jobAreaListToCreate) {
        LOG.info("addJobAreasUserList {}", jobAreaListToCreate);

        for(Integer jobAreaId: jobAreaListToCreate) {
            JobArea jobArea = jobAreaRepository.findById(jobAreaId).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, JOB_AREA_NOT_FOUND));
            JobAreaUser jobAreaUser = new JobAreaUser();
            jobAreaUser.setId(Integer.valueOf(RandomStringUtils.randomAlphanumeric(0, 1000000)));
            jobAreaUser.setUser(user);
            jobAreaUser.setJobArea(jobArea);
            user.addJobAreaUser(jobAreaUser);
        }
    }

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);
}
