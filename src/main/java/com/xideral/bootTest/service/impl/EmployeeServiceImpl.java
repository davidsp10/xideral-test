package com.xideral.bootTest.service.impl;

import com.xideral.bootTest.dto.request.create.NewEmployeeRequest;
import com.xideral.bootTest.dto.request.update.UpdateEmployeeRequest;
import com.xideral.bootTest.dto.resources.EmployeeResource;
import com.xideral.bootTest.entities.Employee;
import com.xideral.bootTest.entities.User;
import com.xideral.bootTest.repository.EmployeeRepository;
import com.xideral.bootTest.repository.UserRepository;
import com.xideral.bootTest.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;
import org.apache.commons.lang3.BooleanUtils;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.xideral.bootTest.constants.MessageConstanst.*;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional(readOnly = true)
    public List<EmployeeResource> findAll() {
        LOG.debug("findAll()");
        List<EmployeeResource> employeeResourceList = new ArrayList<>(0);
        for(Employee employee: employeeRepository.findAll()) {
            employeeResourceList.add(new EmployeeResource(employee));
        }
        return employeeResourceList;
    }

    @Override
    @Transactional(readOnly = true)
    public Employee retrieve(Integer id) {
        return employeeRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, EMPLOYEE_NOT_FOUND));
    }

    @Transactional(readOnly = true)
    public User retrieveUser(Integer id) {
        return userRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
    }

    @Override
    @Transactional
    public Employee persist(NewEmployeeRequest request) throws NoSuchAlgorithmException {
        LOG.debug("persist() request: {}", request);

        Employee employee = new Employee();
        employee.setName(request.getName());
        employee.setFirstName(request.getFirstName());
        employee.setLastName(request.getLastName());
        employee.setBirthDate(new Date(request.getBirthDate()));
        if (BooleanUtils.isTrue(request.getIsUser())) {
            employee.setIsUser(Boolean.TRUE);

            SecureRandom secureRandom = SecureRandom.getInstanceStrong();
            String ramdonChrs = secureRandom.ints(5, 0, chrs.length()).mapToObj(i -> chrs.charAt(i))
                    .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();

            User user = new User();
            user.setName(request.getName());
            user.setPassword(passwordEncoder.encode(ramdonChrs));
            user.setEmployee(employee);
            employee.setUser(user);

            //TODO: Relacionar áreas
        } else {
            employee.setIsUser(Boolean.FALSE);
        }

        employeeRepository.save(employee);

        if(employee.getId() != null)
            LOG.info("Employee {} created", employee.getName());
        else
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Conflict creating Employee");

        return employee;
    }

    @Override
    @Transactional
    public Employee update(UpdateEmployeeRequest request) throws NoSuchAlgorithmException {
        LOG.debug("update() request: {}", request);

        Employee employee = this.retrieve(request.getId());
        employee.setName(request.getName());
        employee.setFirstName(request.getFirstName());
        employee.setLastName(request.getLastName());
        if (request.getBirthDate() != null) employee.setBirthDate(new Date(request.getBirthDate()));
        if (BooleanUtils.isTrue(request.getIsUser())) {
            employee.setIsUser(Boolean.TRUE);
            User user = null;

            if(employee.getUser() != null) {
                user = this.retrieveUser(employee.getUser().getId());
                user.setName(request.getName());
                user.setEmployee(employee);
            } else {
                SecureRandom secureRandom = SecureRandom.getInstanceStrong();
                String ramdonChrs = secureRandom.ints(5, 0, chrs.length()).mapToObj(i -> chrs.charAt(i))
                        .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append).toString();

                user = new User();
                user.setName(request.getName());
                user.setPassword(passwordEncoder.encode(ramdonChrs));
                user.setEmployee(employee);
            }

            employee.setUser(user);
        } else if (BooleanUtils.isFalse(request.getIsUser())) {
            employee.setIsUser(Boolean.FALSE);
        }

        employeeRepository.save(employee);

        if(employee.getId() != null)
            LOG.info("Employee {} updated", employee.getName());
        else
            throw new ResponseStatusException(HttpStatus.CONFLICT, "Conflict updating Employee");

        return employee;
    }

    @Override
    @Transactional
    public void remove(Integer id) {
        LOG.debug("remove() id: {}", id);
        Employee employee = this.retrieve(id);
        employeeRepository.delete(employee);
    }

    private static final Logger LOG = LoggerFactory.getLogger(EmployeeServiceImpl.class);

}
