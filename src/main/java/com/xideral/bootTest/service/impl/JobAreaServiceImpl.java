package com.xideral.bootTest.service.impl;

import com.xideral.bootTest.entities.Employee;
import com.xideral.bootTest.entities.JobArea;
import com.xideral.bootTest.repository.JobAreaRepository;
import com.xideral.bootTest.service.JobAreaService;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.server.ResponseStatusException;

import static com.xideral.bootTest.constants.MessageConstanst.USER_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class JobAreaServiceImpl implements JobAreaService {

    private final JobAreaRepository jobAreaRepository;

    @Override
    @Transactional(readOnly = true)
    public JobArea retrieve(Integer id) {
        return jobAreaRepository.findById(id).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, USER_NOT_FOUND));
    }

    private static final Logger LOG = LoggerFactory.getLogger(JobAreaServiceImpl.class);
}
