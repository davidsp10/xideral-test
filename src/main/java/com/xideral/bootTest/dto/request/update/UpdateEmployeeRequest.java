package com.xideral.bootTest.dto.request.update;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateEmployeeRequest implements Serializable {

    private Integer id;

    @NotBlank
    private String name;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;

    private Long birthDate;

    private Boolean isUser;

}
