package com.xideral.bootTest.dto.request.create;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;

@Data
public class NewEmployeeRequest implements Serializable {

    @NotBlank
    private String name;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    @NotNull
    private Long birthDate;

    private Boolean isUser;

}
