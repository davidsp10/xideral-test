package com.xideral.bootTest.dto.request.create;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class NewUserRequest implements Serializable {

    @NotBlank
    private String name;
    @NotBlank
    private String email;
    @NotBlank
    private String hash;
    @NotEmpty
    private List<Integer> jobAreas;

}

