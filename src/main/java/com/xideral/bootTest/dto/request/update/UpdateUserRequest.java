package com.xideral.bootTest.dto.request.update;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UpdateUserRequest implements Serializable {

    private Integer id;

    @NotBlank
    private String name;
    @NotBlank
    private String email;
    @NotEmpty
    private List<Integer> jobAreas;

}
