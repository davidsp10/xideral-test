package com.xideral.bootTest.dto.resources;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GenericResource {

    private Integer code;
    private String message;

    public GenericResource() {}

    public GenericResource(String message, Integer code) {
        this.message = message;
        this.code = code;
    }

}
