package com.xideral.bootTest.dto.resources;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.xideral.bootTest.constants.DatetimeFormatConstants;
import com.xideral.bootTest.entities.Employee;
import com.xideral.bootTest.util.DateHelper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class EmployeeResource extends GenericResource implements Serializable {

    private Integer id;
    private String name;
    private String firstName;
    private String lastName;
    private String birthDate;
    private Boolean isUser;

    public EmployeeResource(Employee employee) {
        this.id = employee.getId();
        this.name = employee.getName();
        this.firstName = employee.getFirstName();
        this.lastName = employee.getLastName();
        this.birthDate =  DateHelper.tranformToString(employee.getBirthDate(), DatetimeFormatConstants.DATEFORMAT_FULL);
        this.isUser = employee.getIsUser();
    }

    public EmployeeResource(String message, Integer code) {
        setMessage(message);
        setCode(code);
    }
}
