package com.xideral.bootTest.dto.resources;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.xideral.bootTest.entities.JobAreaUser;
import com.xideral.bootTest.entities.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper=true)
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class UserResource extends GenericResource implements Serializable {

    private Integer id;
    private String name;
    private String email;
    private Integer employeeId;
    private List<JobAreaResource> jobAreaList = new ArrayList<>(0);

    public UserResource(User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.email = user.getEmail();
        this.employeeId = user.getEmployee().getId();
        for(JobAreaUser jobAreaUser: user.getJobAreasUser()) {
            JobAreaResource jobAreaResource = new JobAreaResource();
            jobAreaResource.setId(jobAreaUser.getJobArea().getId());
            jobAreaResource.setName(jobAreaUser.getJobArea().getName());
            jobAreaResource.setAbbreviation(jobAreaUser.getJobArea().getAbbreviation());
            this.jobAreaList.add(jobAreaResource);
        }
    }

    public UserResource(String message, Integer code) {
        setMessage(message);
        setCode(code);
    }
}
