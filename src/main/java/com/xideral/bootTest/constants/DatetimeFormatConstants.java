package com.xideral.bootTest.constants;

public final class DatetimeFormatConstants {

	public static final String DATEFORMAT_DASH					= "yyyy-MM-dd";
	public static final String DATEFORMAT_FULL					= "dd/MM/yyyy";

}
