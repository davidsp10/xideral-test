package com.xideral.bootTest.constants;

public final class MessageConstanst {

    public final static String chrs = "0123456789abcdefghijklmnopqrstuvwxyz-_ABCDEFGHIJKLMNOPQRSTUVWXYZ";

    public final static String USER_NOT_FOUND = "Usuario no encontrado";

    public final static String EMPLOYEE_NOT_FOUND = "Empleado no encontrado";

    public final static String JOB_AREA_NOT_FOUND = "JobArea no encontrada";

    public final static String JOB_AREA_USER_NOT_FOUND = "JobAreaUser no encontrada";


}
